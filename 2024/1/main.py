import argparse
from collections import Counter


def main(filename: str):
    with open(filename, "r") as f:
        lst = [l.strip().split("   ") for l in f]
        l, r = list(zip(*lst))

        l = sorted(list(l))
        r = sorted(list(r))

        s = sum(map(lambda aux: abs(int(aux[0]) - int(aux[1])), zip(l, r)))

        print(s)

        c = Counter(r)
        s = sum(map(lambda x: int(x) * int(c[x]), l))

        print(s)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
