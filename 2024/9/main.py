import argparse
from collections import defaultdict
from itertools import combinations
import time


def main(filename: str):
    blocks = list()

    with open(filename, "r") as f:
        file_id = 0
        for idx, char in enumerate(f.read().strip()):
            block_size = int(char)
            aux = file_id if idx % 2 == 0 else -1
            blocks.extend([aux] * block_size)
            file_id = file_id + 1 if aux == file_id else file_id
    first_empty = blocks.index(-1)
    last_non_empty = len(blocks) - 1
    while blocks[last_non_empty] == -1:
        last_non_empty -= 1
    
    while first_empty < last_non_empty:
        blocks[first_empty], blocks[last_non_empty] = blocks[last_non_empty], blocks[first_empty]
        first_empty = blocks.index(-1)
        while blocks[last_non_empty] == -1:
            last_non_empty -= 1

    acc = 0

    for x, file_id in enumerate(blocks[:first_empty]):
        acc += x * file_id

    print(acc)


    blocks = list()
    with open(filename, "r") as f:
        file_id = 0
        for idx, char in enumerate(f.read().strip()):
            block_size = int(char)
            aux = file_id if idx % 2 == 0 else -1
            blocks.extend([aux] * block_size)
            file_id = file_id + 1 if aux == file_id else file_id
    
    first_empty = blocks.index(-1)
    first_non_empty = first_empty
    while blocks[first_non_empty] == -1:
        first_non_empty += 1
    last_non_empty = len(blocks) - 1
    while blocks[last_non_empty] == -1:
        last_non_empty -= 1
    last_empty = last_non_empty
    while blocks[last_empty] == blocks[last_non_empty]:
        last_empty -= 1
    last_empty += 1
    
    while first_empty < last_non_empty:
        while first_non_empty <= last_empty:
            size_empty = first_non_empty - first_empty
            size_file = last_non_empty - last_empty +1
            if size_empty >= size_file:
                blocks[first_empty:first_empty+size_file], blocks[last_empty:last_empty+size_file] = blocks[last_empty:last_empty+size_file], blocks[first_empty:first_empty+size_file]
                break
            else:
                while first_non_empty < len(blocks) and blocks[first_non_empty] != -1:
                    first_non_empty += 1
                first_empty = first_non_empty
                while first_non_empty < len(blocks) and blocks[first_non_empty] == -1:
                    first_non_empty += 1
        first_empty = blocks.index(-1)
        first_non_empty = first_empty
        while first_non_empty < len(blocks) and blocks[first_non_empty] == -1:
            first_non_empty += 1
        last_empty -= 1
        while last_empty > 0 and blocks[last_empty] == -1:
            last_empty -= 1
        last_non_empty = last_empty
        while last_empty > 0 and blocks[last_empty] == blocks[last_non_empty]:
            last_empty -= 1
        last_empty += 1

    acc = 0

    for x, file_id in enumerate(blocks):
        if file_id == -1:
            continue
        acc += x * file_id
    print(acc)
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
