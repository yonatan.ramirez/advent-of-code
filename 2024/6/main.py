import argparse
from copy import deepcopy


def patrol(init_pos, puzzle):
    position = init_pos
    visited_positions = list()
    facing = 'N'
    prev_pos_facing = list()
    nb_lines = len(puzzle)
    nb_col = len(puzzle[0])

    oob = False
    while not oob:
        match facing:
            case 'N':
                if position[0] == 0:
                    oob = True
                    # puzzle[position[0]][position[1]] = 'X'
                    visited_positions.append(position)
                elif puzzle[position[0]-1][position[1]] != '#':
                    visited_positions.append(position)
                    prev_pos_facing.append((position, facing))
                    position = (position[0] - 1, position[1])
                else:
                    prev_pos_facing.append((position, facing))
                    facing = 'E'
            case 'E':
                if position[1] == nb_col-1:
                    oob = True
                    # puzzle[position[0]][position[1]] = 'X'
                    visited_positions.append(position)
                elif puzzle[position[0]][position[1]+1] != '#':
                    visited_positions.append(position)
                    prev_pos_facing.append((position, facing))
                    position = (position[0], position[1]+1)
                else:
                    prev_pos_facing.append((position, facing))
                    facing = 'S'
            case 'S':
                if position[0] == nb_lines-1:
                    oob = True
                    # puzzle[position[0]][position[1]] = 'X'
                    visited_positions.append(position)
                elif puzzle[position[0]+1][position[1]] != '#':
                    visited_positions.append(position)
                    prev_pos_facing.append((position, facing))
                    position = (position[0] + 1, position[1])
                else:
                    prev_pos_facing.append((position, facing))
                    facing = 'W'
            case 'W':
                if position[1] == 0:
                    oob = True
                    # puzzle[position[0]][position[1]] = 'X'
                    visited_positions.append(position)
                elif puzzle[position[0]][position[1]-1] != '#':
                    visited_positions.append(position)
                    prev_pos_facing.append((position, facing))
                    position = (position[0], position[1]-1)
                else:
                    prev_pos_facing.append((position, facing))
                    facing = 'N'
            
        if (position, facing) in prev_pos_facing:
            break
    else:
        return visited_positions
    return None


def main(filename: str):
    position = None
    visited_positions = list()
    puzzle = list()
    with open(filename, "r") as f:
        for i, line in enumerate(f):
            if '^' in line:
                position = (i, line.index('^'))
            puzzle.append(line.strip())
    
    visited_positions = patrol(position, puzzle)

    print(len(set(visited_positions)))

    nb_loops = 0
    for v in set(visited_positions[1:]):
        new_puzzle = deepcopy(puzzle)
        new_puzzle[v[0]] = puzzle[v[0]][:v[1]]+'#'+puzzle[v[0]][v[1]+1:]
        if not patrol(position, new_puzzle):
            nb_loops += 1
    
    print(nb_loops)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
