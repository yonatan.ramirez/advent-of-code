import argparse
import re


def main(filename: str):
    acc = 0

    xmas = re.compile(r'(?=XMAS|SAMX)')

    with open(filename, "r") as f:
        puzzle = [ line.strip() for line in f]

    acc += sum([len(m) for m in [ xmas.findall(line) for line in puzzle]])

    transposed_puzzle = [''.join(line[i] for line in puzzle) for i in range(len(puzzle[0]))]

    acc += sum([len(m) for m in [ xmas.findall(line) for line in transposed_puzzle]])


    right_diag = []
    for i in range(len(puzzle)-3):
        for j in range(len(puzzle[0])-3):
            right_diag.append(
                puzzle[i][j]
                + puzzle[i+1][j+1]
                + puzzle[i+2][j+2]
                + puzzle[i+3][j+3]
            )
    
    acc += sum([len(m) for m in [ xmas.findall(diag) for diag in right_diag]])
    
    left_diag = []
    for i in range(len(puzzle)-3):
        for j in range(len(puzzle[0])-1, 2, -1):
            left_diag.append(
                puzzle[i][j]
                + puzzle[i+1][j-1]
                + puzzle[i+2][j-2]
                + puzzle[i+3][j-3]
            )
    
    acc += sum([len(m) for m in [ xmas.findall(diag) for diag in left_diag]])

    print(acc)

    acc = 0
    for i in range(1, len(puzzle) -1 ):
        for j in range(1, len(puzzle[0]) -1 ):
            if puzzle[i][j] == 'A':
                acc += int(
                    puzzle[i-1][j-1]+puzzle[i][j]+puzzle[i+1][j+1] in ['SAM', 'MAS']
                    and puzzle[i-1][j+1]+puzzle[i][j]+puzzle[i+1][j-1] in ['SAM', 'MAS']
                )
    
    print(acc)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
