import argparse


def get_neighbors(index, grid):
    neighbors = list()
    height = len(grid)
    width = len(grid[0])
    i = index[0]
    j = index[1]
    if i - 1 >= 0:
        neighbors.append(((i - 1, j), grid[i - 1][j]))
    if i + 1 < height:
        neighbors.append(((i + 1, j), grid[i + 1][j]))
    if j - 1 >= 0:
        neighbors.append(((i, j - 1), grid[i][j - 1]))
    if j + 1 < width:
        neighbors.append(((i, j + 1), grid[i][j + 1]))
    return neighbors


def get_nb_hiking_trails(current_level, index, grid):
    if current_level == 9:
        return (index,)
    res = list()
    for elem in map(
        lambda n: get_nb_hiking_trails(n[1], n[0], grid),
        filter(lambda x: x[1] == current_level + 1, get_neighbors(index, grid)),
    ):
        res.extend(elem)
    return res


def main(filename: str):
    with open(filename, "r") as f:
        grid = [list(map(int, list(line.strip()))) for line in f]

    acc_1 = 0
    acc_2 = 0
    for i, line in enumerate(grid):
        for j, elem in enumerate(line):
            if elem == 0:
                routes = list(get_nb_hiking_trails(elem, (i, j), grid))
                acc_2 += len(routes)
                acc_1 += len(set(routes))
    print(acc_1)
    print(acc_2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
