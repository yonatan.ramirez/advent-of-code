import argparse
from itertools import cycle, chain, repeat
import operator as op

p1_ops = [op.add, op.mul]
p2_ops = [op.add, op.mul, lambda x, y: int(op.concat(str(x), str(y)))]

def eq_is_true(test_value, numbers, ops):
    cycle_ops = cycle(ops)
    res = [numbers[0]]
    for i in numbers[1:]:
        res = list(map(lambda x: next(cycle_ops)(x, i), chain.from_iterable(zip(*repeat(res, len(ops))))))
    return test_value in res


def main(filename: str):
    acc = 0
    equations = list()
    with open(filename, "r") as f:
        for line in f:
            test_value, numbers = line.strip().split(": ")
            equations.append((int(test_value), list(map(int, numbers.split()))))

    
    for e in equations:
        if eq_is_true(*e, p1_ops):
            acc += e[0]

    print(acc)

    acc = 0
    for e in equations:
        if eq_is_true(*e, p2_ops):
            acc += e[0]

    print(acc)
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
