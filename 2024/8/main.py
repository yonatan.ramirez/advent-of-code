import argparse
from collections import defaultdict
from itertools import combinations


def get_antinodes(antennas, max_x, max_y):
    tmp = set()
    for a1, a2 in combinations(antennas, 2):
        x_dist = a2[0] - a1[0]
        y_dist = a2[1] - a1[1]
        an1 = (a1[0] - x_dist, a1[1] - y_dist)
        if 0 <= an1[0] < max_x and 0 <= an1[1] < max_y:
            tmp.add(an1)
        an2 = (a2[0] + x_dist, a2[1] + y_dist)
        if 0 <= an2[0] < max_x and 0 <= an2[1] < max_y:
            tmp.add(an2)
    return tmp

def get_antinodes_resonance(antennas, max_x, max_y):
    tmp = set(antennas) if len(antennas) > 1 else set()
    for a1, a2 in combinations(antennas, 2):
        multiplier = 0
        oob = False
        while not oob:
            oob = True
            x_dist = (a2[0] - a1[0]) * multiplier
            y_dist = (a2[1] - a1[1]) * multiplier
            an1 = (a1[0] - x_dist, a1[1] - y_dist)
            if 0 <= an1[0] < max_x and 0 <= an1[1] < max_y:
                tmp.add(an1)
                oob = False
            an2 = (a2[0] + x_dist, a2[1] + y_dist)
            if 0 <= an2[0] < max_x and 0 <= an2[1] < max_y:
                tmp.add(an2)
                oob = False
            multiplier += 1
    return tmp

def main(filename: str):
    antennas = defaultdict(list)
    antinodes = set()

    with open(filename, "r") as f:
        for x, line in enumerate(f):
            for y, char in enumerate(line.strip()):
                if char != ".":
                    antennas[char].append((x, y))
    nb_row = x + 1
    nb_col = y + 1

    for v in antennas.values():
        antinodes |= get_antinodes(v, nb_row, nb_col)
    
    print(len(antinodes))

    antinodes = set()
    for v in antennas.values():
        antinodes |= get_antinodes_resonance(v, nb_row, nb_col)
    
    print(len(antinodes))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
