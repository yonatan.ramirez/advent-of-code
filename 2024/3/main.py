import argparse
import re


def main(filename: str):
    acc = 0
    pattern = re.compile(r'mul\(\d{1,3},\d{1,3}\)')

    with open(filename, "r") as f:
        puzzle = f.read()

    for m in pattern.findall(puzzle):
        x, y = m[4:-1].split(',')
        acc += int(x) * int(y)

    print(acc)

    acc = 0
    replace_regex = re.compile(r"don\'t\(\).*?do\(\)", re.DOTALL)
    puzzle_2 = replace_regex.sub('', puzzle)
    for m in pattern.findall(puzzle_2):
        x, y = m[4:-1].split(',')
        acc += int(x) * int(y)

    print(acc)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
