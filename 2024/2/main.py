import argparse
from functools import reduce
from itertools import combinations


def main(filename: str):
    with open(filename, "r") as f:
        nb_safe_1 = 0
        nb_safe_2 = 0
        for line in f:
            levels = [ int(elem) for elem in line.split()]
            variation = [ levels[i] - levels[i-1] for i in range(1, len(levels))]
            valid = all(map(lambda x: 1 <= abs(x) <= 3, variation)) and abs(reduce(lambda acc, x: acc + x / abs(x), variation, 0)) == len(variation)
            
            nb_safe_1 += int(valid)
            nb_safe_2 += int(valid)

            if valid:
                continue

            for new_levels in combinations(levels, len(levels) - 1):
                new_variation = [ new_levels[i] - new_levels[i-1] for i in range(1, len(new_levels))]
                valid = all(map(lambda x: 1 <= abs(x) <= 3, new_variation)) and abs(reduce(lambda acc, x: acc + x / abs(x), new_variation, 0)) == len(new_variation)
                if valid:
                    nb_safe_2 += 1
                    break

        print(nb_safe_1)
        print(nb_safe_2)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
