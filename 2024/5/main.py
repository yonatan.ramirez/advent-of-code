import argparse
from itertools import pairwise


def main(filename: str):
    page_order_rules = []
    page_to_produce = []
    acc = 0
    acc_2 = 0
    with open(filename, "r") as f:
        for line in f:
            if not line.strip():
                break
            page_order_rules.append(
                list(map(int, line.strip().split('|')))
            )
        for line in f:
            page_to_produce.append(
                list(map(int, line.strip().split(',')))
            )
    
    for to_produce in page_to_produce:
        for x, y in pairwise(to_produce):
            if [x, y] not in page_order_rules:
                break
        else:
            acc += to_produce[len(to_produce)//2]
            continue
        ok = False
        while not ok:
            idx_x, idx_y = to_produce.index(x), to_produce.index(y)
            to_produce[idx_x], to_produce[idx_y] = y, x
            for x, y in pairwise(to_produce):
                if [x, y] not in page_order_rules:
                    break
            else:
                ok = True
        acc_2 += to_produce[len(to_produce)//2]


    print(acc)
    print(acc_2)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
