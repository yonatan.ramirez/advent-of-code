import argparse
from collections import defaultdict

def transform(x):
    if x == 0:
        return 1
    nb_digits = len(str(x))
    if nb_digits % 2 == 0:
        str_x = str(x)
        return [int(str_x[0:nb_digits//2]), int(str_x[nb_digits//2:])]
    return x * 2024


def blink(stones, nb_blinks):
    for i in range(nb_blinks):
        new_stones = defaultdict(int)
        for stone, occurences in stones.items():
            res = transform(stone)
            if isinstance(res, int):
                new_stones[res] += occurences
            else:
                for r in res:
                    new_stones[r] += occurences
        stones = new_stones
    return stones


def main(filename: str):
    stones = defaultdict(int)
    with open(filename, "r") as f:
        for x in f.read().strip().split():
            stones[int(x)] += 1
    print(sum(blink(stones, 25).values()))
    print(sum(blink(stones, 75).values()))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
