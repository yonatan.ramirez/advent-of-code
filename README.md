# Advent of Code

In this repository you will find my solutions to the problems from Advent of Code.

I will use Python 3.12 to solve the problems.

## Get the input data

In order to get the input data and save it into the correct folder, run the following command:
```bash
./get_input.sh -d day -y year
```

where `day` and `year` are the **day** (without leading 0) and **year** of the challenge, respectively.

You will need to put your **session** cookie into a `cookie.txt` file, at the the same location as this script. You can get this cookie from your web browser (right click -> inspect element -> Application tab -> Storage -> Cookies -> session) after authenticating into the advent of code site.

## Execute a challenge

You can easily run a challenge using the following command
```bash
./run.sh -d day -y year -i input
```
where `day` and `year` are the **day** (without leading 0) and **year** of the challenge, and `input` is the **input** file.

### TODO

- [ ] Improve run script
