import argparse
import re

def has_three_vowels(s: str) -> bool:
    return len(re.findall('a|e|i|o|u', s)) >= 3

def has_double_letter(s: str) -> bool:
    for a, b, in zip(s, s[1:]):
        if a == b:
            return True
    return False

def has_substring(s: str) -> str:
    return re.findall('ab|cd|pq|xy', s) == []

def has_same_letter_separated(s: str) -> bool:
    return re.findall(r'([a-z]).\1', s) != []

def has_duplicate_bigram_no_overlap(s: str) -> bool:
    return re.findall(r'([a-z][a-z]).*\1', s) != []

def main(filename: str):
    nb_nice = 0
    with open(filename, "r") as f:
        content = f.readlines()
        for line in content:
            if (
                has_double_letter(line)
                and has_substring(line)
                and has_three_vowels(line)
            ):
                nb_nice += 1

        print(f'Solution 1: {nb_nice}')

        nb_nice = 0
        for line in content:
            if (
                has_same_letter_separated(line)
                and has_duplicate_bigram_no_overlap(line)
            ):
                nb_nice += 1

        print(f'Solution 2: {nb_nice}')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
