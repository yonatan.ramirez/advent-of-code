import argparse
import re
from collections import namedtuple

def parse_instructions(content: list[str]) -> dict[str, list[str]]:
    return { tmp[1]: tmp[0].split() for tmp in [line.strip().split(' -> ') for line in content] }

def compute_instruction(d: dict[str, list], k: str):
    instruction = d[k]
    if type(instruction) is int:
        return instruction
    match len(instruction):
        case 3:
            try:
                l_v = int(d[k][0])
            except ValueError:
                d[k][0] = compute_instruction(d, d[k][0])
                l_v = d[k][0]

            try:
                r_v = int(d[k][2])
            except ValueError:
                d[k][2] = compute_instruction(d, d[k][2])
                r_v = d[k][2]

            match d[k][1]:
                case 'AND':
                    return l_v & r_v
                case 'OR':
                    return l_v | r_v
                case 'LSHIFT':
                    return l_v << r_v
                case 'RSHIFT':
                    return l_v >> r_v
        case 2:
            try:
                int(d[k][1])
            except ValueError:
                d[k][1] = compute_instruction(d, d[k][1])
            return ~d[k][1] & 0xFFFF
        case 1:
            try:
                return int(d[k][0])
            except ValueError:
                return compute_instruction(d, d[k][0])

def main(filename: str):
    with open(filename, "r") as f:
        content = f.readlines()
        instructions = parse_instructions(content)

        for k in instructions.keys():
            instructions[k] = compute_instruction(instructions, k)

        sol_1 = instructions['a']

        instructions = parse_instructions(content)
        instructions['b'] =  sol_1
        for k in instructions.keys():
            instructions[k] = compute_instruction(instructions, k)

        sol_2 = instructions['a']

        print(f'Solution 1 : {sol_1}')
        print(f'Solution 2 : {sol_2}')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
