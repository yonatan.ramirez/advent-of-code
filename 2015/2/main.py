import argparse
from operator import mul
from functools import reduce

def compute_wrapping_area(l: int, w: int, h: int) -> int:
    side_surface = sorted([l*w, w*h, l*h])
    return sum([2*elem for elem in side_surface]) + side_surface[0]

def compute_ribbon_length(l: int, w: int, h: int) -> int:
    sides = sorted([l,w,h])
    return sum(sides[:2])*2 + reduce(mul,sides)

def main(filename: str):
    with open(filename, "r") as f:
        content = f.readlines()
        sum_wrap = 0
        sum_ribbon = 0
        for line in content:
            sum_wrap += compute_wrapping_area(*[int(elem) for elem in line.split('x')])
            sum_ribbon += compute_ribbon_length(*[int(elem) for elem in line.split('x')])
        print(f'Solution 1: {sum_wrap}')
        print(f'Solution 2: {sum_ribbon}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
