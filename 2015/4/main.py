import argparse
from hashlib import md5

def main(filename: str):
    with open(filename, "r") as f:
        secret = f.readline().strip()
        nonce = 0
        while not md5(f'{secret}{nonce}'.encode()).hexdigest().startswith('00000'):
            nonce += 1

        print(f'Solution 1: {nonce}')

        nonce = 0
        while not md5(f'{secret}{nonce}'.encode()).hexdigest().startswith('000000'):
            nonce += 1

        print(f'Solution 2: {nonce}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
