import argparse

def main(filename: str):
    with open(filename, "r") as f:
        content = f.readline().strip()
        opening = content.count('(')
        closing = content.count(')')
        print(f'Solution 1: {opening-closing}')

        sum = 0
        for i in range(0, len(content)):
            match content[i]:
                case '(':
                    sum +=1
                case ')':
                    sum -=1
                    if sum < 0:
                        print(f'Solution 2: {i+1}')
                        break


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
