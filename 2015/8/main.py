import argparse
import json
from collections import namedtuple

def main(filename: str):
    with open(filename, "r") as f:
        nb_code_rep = 0
        nb_mem_rep = 0
        for l in f:
            s = bytes(l.strip(), 'utf8').decode('unicode_escape')
            nb_code_rep += (len(l.strip()))
            nb_mem_rep += len(str(s)) - 2

    print(f'Solution 1 : {nb_code_rep - nb_mem_rep}')

    with open(filename, "r") as f:
        nb_code_rep = 0
        nb_new_enc = 0
        for l in f:
            nb_code_rep += (len(l.strip()))
            nb_new_enc += len(json.dumps(l.strip()))

        print(f'Solution 2 : {nb_new_enc - nb_code_rep }')
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
