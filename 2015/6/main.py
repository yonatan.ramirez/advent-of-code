import argparse
import re
from collections import namedtuple

Light = namedtuple('Light', ['x', 'y'])

def main(filename: str):
    with open(filename, "r") as f:
        content = f.readlines()
        p = re.compile(r'(off|on|toggle) (\d+),(\d+).* (\d+),(\d+)')
        lights : dict[Light, int]= dict()
        for line in content:
            action, xs, ys, xe, ye = p.findall(line)[0]
            for x in range(int(xs), int(xe)+1):
                for y in range(int(ys), int(ye)+1):
                    match action:
                        case 'off':
                            lights[Light(x,y)] = 0
                        case 'on':
                            lights[Light(x,y)] = 1
                        case 'toggle':
                            lights[Light(x,y)] = 1 if lights.get(Light(x,y),0) == 0 else 0

        print(f'Solution 1: {sum(lights.values())}')

        lights : dict[Light, int]= dict()
        for line in content:
            action, xs, ys, xe, ye = p.findall(line)[0]
            for x in range(int(xs), int(xe)+1):
                for y in range(int(ys), int(ye)+1):
                    match action:
                        case 'off':
                            lights[Light(x,y)] = 0 if lights.get(Light(x,y),0) == 0 else lights[Light(x,y)] - 1
                        case 'on':
                            lights[Light(x,y)] = lights.get(Light(x,y),0) + 1
                        case 'toggle':
                            lights[Light(x,y)] = lights.get(Light(x,y),0) + 2

        print(f'Solution 2: {sum(lights.values())}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
