import argparse
from collections import namedtuple, Counter
Point = namedtuple('Point', ['x', 'y'])

def next_position(current: Point, direction: str) -> Point:
    match direction:
        case '^':
            return Point(current.x, current.y+1)
        case '>':
            return Point(current.x+1, current.y)
        case 'v':
            return Point(current.x, current.y-1)
        case '<':
            return Point(current.x-1, current.y)

def main(filename: str):
    current = Point(0,0)
    visited = Counter([current])
    with open(filename, "r") as f:
        content = f.readline()
        for direction in content:
            current = next_position(current, direction)
            visited[current] += 1

        print(f'Solution 1: {len(set(visited))}')

        current = [Point(0,0),Point(0,0)]
        visited = Counter(current)
        for index, direction in enumerate(content):
            current[index%2] = next_position(current[index%2], direction)
            visited[current[index%2]] += 1
        print(f'Solution 2: {len(set(visited))}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
