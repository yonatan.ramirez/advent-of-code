import argparse
import re

str_digit_to_string = {
    "zero": 0,
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
}


def main(filename: str):
    with open(filename, "r") as f:
        sum = 0
        for line in f:
            digit_list = []
            for k, v in str_digit_to_string.items():
                start_indexes = [m.start() for m in re.finditer(k, line)]
                for index in start_indexes:
                    digit_list.append((index, v))
            digit_list.extend(
                [(index, int(c)) for index, c in enumerate(line) if c.isdigit()]
            )
            if digit_list:
                digit_list.sort(key=lambda x: x[0])
                to_add = digit_list[0][1] * 10 + digit_list[-1][1]
                sum += to_add
        print(sum)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    main(args.filename)
