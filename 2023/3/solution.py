import argparse

def get_numbers_in_row(row: list[str]):
    numbers = list()
    start = None
    for index, char in enumerate(row):
        if char.isdigit() and start is None:
            start = index
        if not char.isdigit() and start is not None:
            numbers.append(
                {"start": start, "end": index, "value": int(row[start:index])}
            )
            start = None
    if start:
        numbers.append(
            {"start": start, "end": index, "value": int(row[start:index+1])}
        )
    return numbers


def symbol_in_number_neighbours(row, col, schematic):
    h, w = (len(schematic[0]), len(schematic))
    for i in [row - 1, row, row + 1]:
        for j in [col - 1, col, col + 1]:
            if i == row and j == col:
                continue
            if 0 <= i < w and 0 <= j < h:
                c = schematic[i][j]
                if not c.isdigit() and c != ".":
                    return True
    return False


def is_valid_number(number, row_num, schematic):
    return any(
        [
            symbol_in_number_neighbours(row_num, cell, schematic)
            for cell in range(number["start"], number["end"])
        ]
    )

def get_stars_in_row(row):
    stars = list()
    for index, c in enumerate(row):
        if c == '*':
            stars.append(index)
    return stars


def is_gear(row, col, schematic):
    return count_surrounding_numbers(row, col, schematic) == 2

def count_surrounding_numbers(row, col, schematic):
    h, w = (len(schematic[0]), len(schematic))
    nb_numbers = 0
    for i in [row - 1, row, row + 1]:
        is_num = False
        for j in [col - 1, col, col + 1]:
            if i == row and j == col:
                if is_num:
                    nb_numbers += 1
                    is_num = False
                continue
            if 0 <= i < w and 0 <= j < h:
                if schematic[i][j].isdigit():
                    is_num = True
                else:
                    if is_num:
                        nb_numbers += 1
                        is_num = False
        if is_num:
            nb_numbers += 1
    return nb_numbers

def sol_1(filename: str):
    with open(filename, "r") as f:
        schematic = []
        sum = 0
        for line in f:
            schematic.append(line.strip())
        for index, row in enumerate(schematic):
            numbers = get_numbers_in_row(row)
            for number in numbers:
                if is_valid_number(number, index, schematic):
                    sum += number['value']
        print(sum)

def get_gear_ratio(row, col, schematic):
    h, w = (len(schematic[0]), len(schematic))
    nums = set()
    for i in [row - 1, row, row + 1]:
        for j in [col - 1, col, col + 1]:
            if i == row and j == col:
                continue
            if 0 <= i < w and 0 <= j < h:
                if schematic[i][j].isdigit():
                    nums.add(parse_number(i, j, schematic))
    nums = list(nums)
    if len(nums) == 1:
        return nums[0] * nums[0]
    return nums[0] * nums[1]

def parse_number(row, col, schematic):
    w = len(schematic[0])
    start = col
    end = col
    while 0 <= start and schematic[row][start].isdigit():
        start -= 1
    start += 1

    while end < w and schematic[row][end].isdigit():
        end += 1

    return int(schematic[row][start:end])


def sol_2(filename: str):
    with open(filename, "r") as f:
        schematic = []
        sum = 0
        for line in f:
            schematic.append(line.strip())
        for index, row in enumerate(schematic):
            stars = get_stars_in_row(row)
            for star in stars:
                if is_gear(index, star, schematic):
                    sum += get_gear_ratio(index, star, schematic)
        print(sum)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    subparsers = parser.add_subparsers(required=True)
    solution_a = subparsers.add_parser('sol_1')
    solution_a.set_defaults(func=sol_1)
    solution_b = subparsers.add_parser('sol_2')
    solution_b.set_defaults(func=sol_2)
    args = parser.parse_args()

    fun_args = vars(args).copy()
    del fun_args['func']
    args.func(**fun_args)
