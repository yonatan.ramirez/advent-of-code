import argparse
from collections import defaultdict
from itertools import accumulate
import operator

def parse_dices(dices: list[str]) -> dict:
    data = {}
    for dice in dices.split(','):
        nb, color = dice.strip().split(' ')
        data[f'nb_{color}'] = int(nb)
    return data

def possible_game(
    max_red: int,
    max_green: int,
    max_blue: int,
    nb_red: int = 0,
    nb_green: int = 0,
    nb_blue: int = 0,
) -> bool:
    return (
        nb_red <= max_red
        and nb_green <= max_green
        and nb_blue <= max_blue
    )


def sol_1(filename: str, max_red: int, max_green: int, max_blue: int):
    with open(filename, 'r') as f:
        sum = 0
        for line in f:
            first_split = line.split(':')
            game_id = int(first_split[0].split(' ')[1])
            subsets = first_split[1].split(';')
            if all([possible_game(max_red, max_green, max_blue, **parse_dices(dices)) for dices in subsets]):
                sum += game_id
        print(sum)


def sol_2(filename: str):
    with open(filename, 'r') as f:
        sum = 0
        for line in f:
            first_split = line.split(':')
            subsets = first_split[1].split(';')
            picked_dices = defaultdict(list)
            for dices in subsets:
                for color, value in parse_dices(dices).items():
                    picked_dices[color].append(value)
            minimal_values = [max(values) for values in picked_dices.values()]
            sum += list(accumulate(minimal_values, operator.mul))[-1]
        print(sum)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    subparsers = parser.add_subparsers(required=True)
    solution_a = subparsers.add_parser('sol_1')
    solution_a.add_argument('-r', '--max_red', type=int, required=True)
    solution_a.add_argument('-g', '--max_green', type=int, required=True)
    solution_a.add_argument('-b', '--max_blue', type=int, required=True)
    solution_a.set_defaults(func=sol_1)
    solution_b = subparsers.add_parser('sol_2')
    solution_b.set_defaults(func=sol_2)
    args = parser.parse_args()

    fun_args = vars(args).copy()
    del fun_args['func']
    args.func(**fun_args)
