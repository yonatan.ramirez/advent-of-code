import argparse
import re
from collections import Counter


def sol_1(filename: str):
    with open(filename, "r") as f:
        sum = 0
        for line in f:
            first_split = line.split(":")
            winning_numbers, my_numbers = first_split[1].split("|")
            winning_numbers = re.sub(r"\s+", " ", winning_numbers).strip().split(" ")
            my_numbers = re.sub(r"\s+", " ", my_numbers).strip().split(" ")
            nb_matching = len(set(winning_numbers).intersection(my_numbers))
            if nb_matching:
                sum += 2 ** (nb_matching-1)
        print(sum)


def sol_2(filename: str):
    with open(filename, "r") as f:
        scratchcards = Counter()
        for index, line in enumerate(f):
            scratchcards.update([index])
            first_split = line.split(":")
            winning_numbers, my_numbers = first_split[1].split("|")
            winning_numbers = re.sub(r"\s+", " ", winning_numbers).strip().split(" ")
            my_numbers = re.sub(r"\s+", " ", my_numbers).strip().split(" ")
            nb_matching_cards = len(set(winning_numbers).intersection(my_numbers))
            for i in range(1, nb_matching_cards+1):
                scratchcards[index+i] += scratchcards[index]
        print(scratchcards.total())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    subparsers = parser.add_subparsers(required=True)
    solution_a = subparsers.add_parser("sol_1")
    solution_a.set_defaults(func=sol_1)
    solution_b = subparsers.add_parser("sol_2")
    solution_b.set_defaults(func=sol_2)

    args = parser.parse_args()

    fun_args = vars(args).copy()
    del fun_args["func"]
    args.func(**fun_args)
